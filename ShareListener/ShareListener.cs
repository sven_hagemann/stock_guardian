﻿using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceProcess;
using System.Timers;

namespace ShareListener
{
    //C:\WINDOWS\Microsoft.NET\Framework64\v4.0.30319\installutil ShareListener.exe
    //C:\WINDOWS\Microsoft.NET\Framework64\v4.0.30319\installutil /u ShareListener.exe
    public partial class ShareListener : ServiceBase
    {
        private WebClient webClient;

        public ShareListener()
        {
            InitializeComponent();
            webClient = new WebClient();
            webClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        }

        protected override void OnStart(string[] args)
        {
            // Set up a timer that triggers every 15 seconds.
            Timer timer = new Timer();
            timer.Interval = 15000; // 15 seconds
            timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        protected override void OnStop()
        {
        }

        private void OnTimer(object sender, ElapsedEventArgs e)
        {
            var response = webClient.DownloadString("https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=VSC.FRK&apikey=2S87NN268JGZW13T");
            var price = JObject.Parse(response)["Global Quote"]["05. price"];
            //Process.Start(@"C:\MyBatchFiles\MyBatch.bat");
            //var currDir = Directory.GetCurrentDirectory();

            MailMessage mailMessage = new MailMessage
            {
                From = new MailAddress("Aktienwächter"),
                Subject = "4SC-Aktien hat sich verändert.",
                Body = "Schau schnell nach: https://www.finanzen.net/aktien/4sc-aktie",
                IsBodyHtml = true
            };
            mailMessage.To.Add(new MailAddress("s.hro@outlook.de"));
            mailMessage.To.Add(new MailAddress("Hagemann@optimal-systems.de"));

            NetworkCredential NetworkCred = new NetworkCredential
            {
                UserName = mailMessage.From.Address,
                Password = ""
            };

            SmtpClient smtp = new SmtpClient
            {
                EnableSsl = false,
                UseDefaultCredentials = true,
                Credentials = NetworkCred,
                Port = 3535
            };
            smtp.Send(mailMessage); //sending Email  
        }
    }
}
